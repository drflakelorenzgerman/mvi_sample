package ir.sass.mvi_sample.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped
import ir.sass.mvi_sample.api.Api
import ir.sass.mvi_sample.api.ApiRepository
import ir.sass.mvi_sample.api.RetrofitBuilder

@Module
@InstallIn(ViewModelComponent::class)
object ViewModelModules {

    @Provides
    @ViewModelScoped
    fun provideApi() : Api {
        return RetrofitBuilder.getRetrofit().create(Api::class.java)
    }

    @Provides
    @ViewModelScoped
    fun provideRepo(api : Api) : ApiRepository = ApiRepository(api)

}