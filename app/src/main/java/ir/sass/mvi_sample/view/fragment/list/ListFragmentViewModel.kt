package ir.sass.mvi_sample.view.fragment.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import ir.sass.mvi_sample.api.ApiRepository
import ir.sass.mvi_sample.model.base.GeneralDataState
import ir.sass.mvi_sample.model.data.Post
import ir.sass.mvi_sample.view.base.BaseViewModel
import ir.sass.mvi_sample.view.base.SingleLiveEvent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ListFragmentViewModel @Inject constructor(var apiRepository: ApiRepository)
    : BaseViewModel<ListViewState>(){

    protected val _viewEffects: SingleLiveEvent<ViewEffect> = SingleLiveEvent()
    val viewEffects: LiveData<ViewEffect> = _viewEffects

    fun action(intent: ListFragmentIntent){
        when(intent){
            is ListFragmentIntent.GetPosts->getPosts()
            is ListFragmentIntent.GetUser->getUser(intent.id)
            is ListFragmentIntent.ClickOnPost->clickOnPost(intent.post)
        }
    }

    private fun getPosts(){
        runRequest("Posts") {
            val data = apiRepository.getPosts()
            val newState = ListViewState(posts = data)
            _dataStates.postValue(GeneralDataState.Response(newState,"Posts"))
        }
    }

    private fun getUser(id : Int){
        runRequest("Users") {
            val data = apiRepository.getUser(id)
            val newState = ListViewState(user = data[0])
            _dataStates.postValue(GeneralDataState.Response(newState,"Users"))
        }
    }

    private fun clickOnPost(post : Post){
        _viewEffects.postValue(ViewEffect.NavigateToDetailsFragment(post))
//        _viewEffects.postValue(ViewEffect.None)
    }
}