package ir.sass.mvi_sample.view.fragment.details

import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import ir.sass.mvi_sample.R
import ir.sass.mvi_sample.databinding.FragmentDetailsBinding
import ir.sass.mvi_sample.view.base.BaseFragment

class DetailsFragment(override val layout: Int = R.layout.fragment_details) : BaseFragment<FragmentDetailsBinding>(){

    val args by navArgs<DetailsFragmentArgs>()

    override fun binding() {
        dataBinding.txt.text = args.data.body
    }
}