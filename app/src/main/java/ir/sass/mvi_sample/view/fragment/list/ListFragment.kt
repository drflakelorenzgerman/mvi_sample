package ir.sass.mvi_sample.view.fragment.list

import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import ir.sass.mvi_sample.R
import ir.sass.mvi_sample.databinding.FragmentListBinding
import ir.sass.mvi_sample.model.base.GeneralDataState
import ir.sass.mvi_sample.view.base.BaseFragment
import ir.sass.mvi_sample.view.base.BaseViewModel

@AndroidEntryPoint
class ListFragment(override val layout: Int = R.layout.fragment_list) : BaseFragment<FragmentListBinding>() {

    private val viewModel : ListFragmentViewModel by viewModels()

    lateinit var adapter : ListFragmentAdapter

    override fun binding() {
        viewModel.viewEffects.observe(viewLifecycleOwner, Observer {
            when(it){
                is ViewEffect.NavigateToDetailsFragment->{
                    findNavController().navigate(ListFragmentDirections.actionListFragmentToDetailsFragment(it.post))
                }
                else->{
                    // extra
                }
            }
        })

        viewModel.dataStates.observe(viewLifecycleOwner, Observer {
            when(it){
                is GeneralDataState.Response->{

                    if(BaseViewModel.jobCounter == 0)
                        dataBinding.progress.visibility = View.GONE
                    dataBinding.list.visibility = View.VISIBLE

                    it.toData()?.posts?.let {
                        adapter = ListFragmentAdapter(it){
                            viewModel.action(ListFragmentIntent.ClickOnPost(it))
                        }
                        dataBinding.list.adapter = adapter
                        dataBinding.list.layoutManager = LinearLayoutManager(requireContext())
                    }

                    it.toData()?.user?.let {
                        dataBinding.user.text = it.email
                    }
                }
                is GeneralDataState.Failed->{
                    Toast.makeText(requireContext(),it.error.localizedMessage.toString(),Toast.LENGTH_LONG).show()
                    if(BaseViewModel.jobCounter == 0)
                        dataBinding.progress.visibility = View.GONE
                }
                is GeneralDataState.Loading->{
                    dataBinding.progress.visibility = View.VISIBLE
                    dataBinding.list.visibility = View.GONE
                }
            }


        })

        viewModel.action(ListFragmentIntent.GetUser(1))
        viewModel.action(ListFragmentIntent.GetPosts)

    }
}