package ir.sass.mvi_sample.view.fragment.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ir.sass.mvi_sample.databinding.ItemListFragmentBinding
import ir.sass.mvi_sample.model.data.Post

class ListFragmentAdapter(var list : List<Post> , var click : (item : Post) -> Unit) : RecyclerView.Adapter<ListFragmentAdapter.VH>() {

    class VH(var binding : ItemListFragmentBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH = VH(
        ItemListFragmentBinding.inflate(LayoutInflater.from(parent.context)))

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.binding.text.text = list[position].title

        holder.binding.text.setOnClickListener {
            click.invoke(list[position])
        }
    }

    override fun getItemCount(): Int = list.size
}