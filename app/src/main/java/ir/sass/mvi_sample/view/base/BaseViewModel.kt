package ir.sass.mvi_sample.view.base

import android.util.Log
import androidx.lifecycle.*
import ir.sass.mvi_sample.model.base.GeneralDataState
import ir.sass.mvi_sample.model.data.Post
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.IO


open class BaseViewModel<T> : ViewModel() {
    protected val _dataStates: MutableLiveData<GeneralDataState<T>> = MutableLiveData()
    val dataStates: LiveData<GeneralDataState<T>> = _dataStates

    protected fun getViewState() : T? = _dataStates.value?.toData()

    companion object{
        var jobCounter = 0
    }

    fun runRequest(nameTag : String = "Nothing",req : suspend() -> Unit) {
        jobCounter++
        viewModelScope.launch {
            val currentState = getViewState()
            try {
                _dataStates.postValue(GeneralDataState.Loading(currentState,"Loading...",nameTag))
                req()
                jobCounter--
            }catch (exception: Exception) {
                delay(100)
                jobCounter--
                _dataStates.postValue(GeneralDataState.Failed(currentState, exception,nameTag))
            }
        }
    }
}