package ir.sass.mvi_sample.view.fragment.list

import ir.sass.mvi_sample.model.data.Post

sealed class ListFragmentIntent {
    class GetUser(var id: Int) : ListFragmentIntent()
    object GetPosts : ListFragmentIntent()
    class ClickOnPost(var post : Post) : ListFragmentIntent()
}