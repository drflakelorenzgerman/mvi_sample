package ir.sass.mvi_sample.view.fragment.list

import ir.sass.mvi_sample.model.data.Post
import ir.sass.mvi_sample.model.data.User

data class ListViewState(
    val posts : List<Post>? = null,
    val user : User? = null
)