package ir.sass.mvi_sample.view.fragment.list

import ir.sass.mvi_sample.model.data.Post

sealed class ViewEffect {
    class NavigateToDetailsFragment(var post : Post) : ViewEffect()
}