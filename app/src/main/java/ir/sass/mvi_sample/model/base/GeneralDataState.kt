package ir.sass.mvi_sample.model.base

import java.lang.Exception


sealed class GeneralDataState<out ViewState>{

    data class Response<ViewState>(val data : ViewState?,val tag : String = "Nothing") : GeneralDataState<ViewState>()
    data class Failed<ViewState>(val data : ViewState?,val error : Exception,val tag : String = "Nothing") : GeneralDataState<ViewState>()
    data class Loading<ViewState>(val data : ViewState?,val message : String,val tag : String = "Nothing") : GeneralDataState<ViewState>()

    fun toData() : ViewState? = when(this){
        is  Response-> this.data
        is  Failed-> this.data
        is  Loading-> this.data
    }

    fun type() : String = when(this){
        is  Response-> "Response"
        is  Failed-> "Failed"
        is  Loading-> "Loading"
    }

    fun tag() : String = when(this){
        is  Response-> tag
        is  Failed-> tag
        is  Loading-> tag
    }

}
