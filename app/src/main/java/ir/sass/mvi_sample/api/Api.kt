package ir.sass.mvi_sample.api

import ir.sass.mvi_sample.model.data.Post
import ir.sass.mvi_sample.model.data.User
import retrofit2.http.GET
import retrofit2.http.Query

interface Api {
    @GET("posts")
    suspend fun getPosts() : List<Post>

    @GET("users")
    suspend fun getUser(@Query("id") id : Int) : List<User>
}