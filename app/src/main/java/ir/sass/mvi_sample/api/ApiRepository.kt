package ir.sass.mvi_sample.api

import ir.sass.mvi_sample.model.data.Post
import javax.inject.Inject

class ApiRepository @Inject constructor(var api : Api){

    suspend fun getPosts()  =  api.getPosts()

    suspend fun getUser(id : Int) = api.getUser(id)

}